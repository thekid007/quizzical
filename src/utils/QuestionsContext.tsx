import { createContext, useEffect, useState, FC, ReactNode } from "react";
import callApi from "./methods";

// Created an interface that contains a "children" element of type ReactNode.
interface Props {
  children: ReactNode;
}

// Create the context for the questions that will be fetched from the API.
export const QuestionsContext = createContext<any[]>([]);

// Create a provider for the context.
const QuestionsProvider: FC<Props> = ({ children }) => {
  // Create a variable in state that will store the question data.
  const [questions, setQuestions] = useState("");

  // Fetch the question data on first page load.
  useEffect(() => {
    async function getQuestions() {
      const res = await callApi("GET");
      const body = await res.json();
      setQuestions(body.results);
    }
    getQuestions();
  }, []);

  // Return the provider using the question data as the value.
  return (
    <QuestionsContext.Provider value={questions as any}>
      {children}
    </QuestionsContext.Provider>
  );
};

export default QuestionsProvider;
