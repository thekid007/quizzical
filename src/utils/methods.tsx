const baseUrl = "https://opentdb.com/api.php?amount=50&type=multiple";

// A function that makes a fetch request to the api. It can be used to GET or POST information to the api, depending on the parameteres that are sent.
const callApi = async (method: string, body?: object) => {
  const res = await fetch(baseUrl, {
    body: JSON.stringify(body),
    method: method,
  });

  return res;
};

export default callApi;
