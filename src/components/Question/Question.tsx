import React from "react";
import "./question.css";

export default function Question(props: any) {
  const possibleAnswers = props.answers.map((answer: any) => {
    return (
      <button
        className={props.selected ? "selectedAnswer" : "answer"}
        onClick={() => {
          props.onSelectAnswer(answer);
        }}
      >
        {answer}
      </button>
    );
  });
  return (
    <div className="question">
      <h1 className="questionText">{props.text}</h1>
      <div className="answersContainer">{possibleAnswers}</div>
    </div>
  );
}
