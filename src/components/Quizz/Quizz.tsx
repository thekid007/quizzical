import { useContext, useEffect, useState } from "react";
import Question from "../Question/Question";
import "./quizz.css";
import { QuestionsContext } from "../../utils/QuestionsContext";

export default function Quizz() {
  // Create a state for a boolean that will render the first screen depending on its value.
  const [buttonClicked, setButtonClicked] = useState(false);

  // Use QuestionContext that contains all the question data.
  const quizData = useContext(QuestionsContext);
  // Create a variable in state that will generate a random question and its possible answers from the quizData variable.
  const [question, setQuestion] = useState<any[]>([]);

  // This function generates 5 random questions and saves them in state.
  function generateQuestions() {
    // Generate 5 random numbers which will be used as indexes of the questions saved in state.
    for (let i = 0; i < 5; i++) {
      const randomQuestion = Math.floor(Math.random() * quizData.length);

      // For each question, get the text, answers and correct answer and add it to state.
      setQuestion((current: any) => [
        ...current,
        {
          question: quizData[randomQuestion].question,
          answers: [
            ...quizData[randomQuestion].incorrect_answers,
            quizData[randomQuestion].correct_answer,
          ],
          correctAns: quizData[randomQuestion].correct_answer,
        },
      ]);
    }
  }

  // Handle answering questions. Compare the selected answer with the correct answer and display whether it is correct or not.
  function handleAnswer(questionText: string, answer: string) {
    // Get the index of the question from the state variable.
    const questionIndex = question
      .map((data) => data.question)
      .indexOf(questionText);

    // Check if the answer to the selected question is the same as the correct answer.
    if (question[questionIndex].correctAns === answer) {
      console.log("Correct!");
    } else {
      console.log("Wrong!");
    }
  }

  function selectAnswer() {}

  // Generate a Question component for every question object in state.
  const questionItems = question.map((data) => {
    return (
      <Question
        text={data.question}
        answers={data.answers}
        onSelectAnswer={selectAnswer}
        selected={false}
      />
    );
  });

  /* onHandleAnswer={(answer) => {
    handleAnswer(data.question, answer);
  }}*/

  // Button onClick function, change the value of the boolean in state to true. The overlay will disappear.
  function onButtonClick() {
    setButtonClicked(true);
    generateQuestions();
  }
  return (
    /* Creating the main div for the overlay. The class name is dependant on
      the value of the boolean. If the button has been clicked, display: none. 
      Wrap everything in a <main> tag. */
    <main>
      <div className={buttonClicked ? "introScreenNone" : "introScreen"}>
        <h1 className="quizzical">Quizzical</h1>
        <p className="quizzicalParagraph">Some description if needed</p>
        <button className="startButton" onClick={onButtonClick}>
          Start quiz
        </button>
      </div>
      <div className={buttonClicked ? "quizWindow" : "quizWindowNone"}>
        {questionItems}
        <button className="checkAnswers">Check Answers</button>
      </div>
    </main>
  );
}
