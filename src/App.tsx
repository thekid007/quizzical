import "./App.css";
import { Routes, BrowserRouter, Route } from "react-router-dom";
import Quizz from "./components/Quizz/Quizz";
import QuestionsProvider from "./utils/QuestionsContext";
export default function App() {
  return (
    /* Creating the main div for the overlay. The class name is dependant on
      the value of the boolean. If the button has been clicked, display: none. 
      Wrap everything in a <main> tag. */

    <BrowserRouter>
      <div className="App">
        <QuestionsProvider>
          <Routes>
            <Route path={"/"} element={<Quizz />} />
          </Routes>
        </QuestionsProvider>
      </div>
    </BrowserRouter>
  );
}
